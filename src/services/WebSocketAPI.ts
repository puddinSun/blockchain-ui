import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import * as SockJS from 'sockjs-client';
import * as Stomp from 'stompjs';

import { AppComponent } from '../app/app.component';

@Injectable({providedIn: "root"})
export class WebSocketAPI {
  stompClient: any;
  msg$ = new BehaviorSubject<string>('');

  constructor() {
  }

  _connect() {
    console.log('Initializing a websocket connection ...');
    let ws = new SockJS('http://localhost:8888/ws');
    this.stompClient = Stomp.over(ws);
    const _this = this;

    _this.stompClient.connect(
      {},
      function (frame: any) {
        if (frame.command == 'CONNECTED') {
          _this.stompClient.subscribe('/topic/process', function (event: any) {
            _this.onMessageReceive(event);
          });
        }
      },
      _this.errorCallback
    );
  }

  onMessageReceive(event: any) {
    console.log('Message recieved from server :: ' + event);
    this.msg$.next(event.body);
  }

  errorCallback(error: any) {
    console.log('Error callBack -> ' + error);
    setTimeout(() => {
      this._connect();
    }, 5000);
  }

  _send(message: string) {
    console.log('Sending payload to the topic ==> ', message);
    this.stompClient.send('/app/prices', {}, message);
  }

  _disconnect() {
    if (this.stompClient !== null) {
      console.log('Disconnected');
      this.stompClient.disconnect();
    }
  }
}
