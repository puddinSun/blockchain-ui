import { Component, OnDestroy, OnInit } from '@angular/core';
import { distinctUntilChanged, Subject, takeUntil } from 'rxjs';

import { WebSocketAPI } from '../services/WebSocketAPI';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent implements OnInit, OnDestroy {
  title = 'real-time-crypto';

  greeting = "Sun";
  name: string = "";
  destroy$ = new Subject();
  constructor(private webSocketAPI: WebSocketAPI) {}

  ngOnInit() {
    this.webSocketAPI.msg$.pipe(distinctUntilChanged(), takeUntil(this.destroy$))
    .subscribe(event => this.handleMessage(event))
  }

  connect() {
    this.webSocketAPI?._connect();
  }

  disconnect() {
    this.webSocketAPI?._disconnect();
  }

  sendMessage() {
    this.webSocketAPI?._send(this.name);
  }

  handleMessage(message: string) {
    console.log("Handle UI change ==> ", message)
    this.greeting = message;
  }

  ngOnDestroy(): void {
    this.destroy$.next(true);
  }
}
