
# Stage 1: Build app and it's dependencies
FROM node:16-alpine
WORKDIR /app
COPY . . 
RUN npm install
RUN npm run build

# Stage 2: Copy build files to nginx server
FROM nginx:alpine
COPY --from=node /app/dist/real-time-crypto/* /usr/share/nginx/html/